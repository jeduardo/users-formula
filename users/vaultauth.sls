# vim: sts=2 ts=2 sw=2 et ai
{% from "users/map.jinja" import users with context %}

{%- if 'vault-user-manager' in salt['grains.get']('inventory:roles') %}

{%- for name, user in pillar.get('users', {}).items() 
  if user.absent is not defined or not user.absent %}

{% set vault_addr = salt['grains.get']('environment:vault:addr')  %}

{{ vault_addr }}/v1/ssh-client-signer/roles/{{ name }}:
  http.query:
    - method: 'POST'
    - status: 204
    - data_render: True
    - header_file: /tmp/headers.txt
    - data: '{"allow_user_certificates": true, "allowed_users": "{{ name }}", "default_extensions": [ {"permit-pty": ""} ], "key_type": "ca", "default_user": "{{ name }}", "ttl": "1m0s"}'
    - header_render: True
    - cookies: True
    - persist_session: True
    - verify_ssl: False
    - proxy_host: ""
    - proxy_port: ""
{%- endfor %}

{%- endif %}